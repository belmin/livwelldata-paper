
This folder should contain the raw climate data downloaded from the CRU, however, we have git ignored the data because they are very big. 

The monthly gridded CRU climate data can be accessed and downloaded here (CRU TS v. 4.05):  https://crudata.uea.ac.uk/cru/data/hrg/. 
