# LivWelldata paper

This is the companion repository for the paper: "Livwell: a sub-national database on the Living conditions of Women and their Well-being for 52 countries" by Belmin and colleagues.

The database can be found at [here](https://gitlab.pik-potsdam.de/belmin/livwelldata-paper/-/tree/main/analysis/data/derived_data/database.csv), as well on Zenodo at the permanent DOI: https://doi.org/10.5281/zenodo.5821532

The code to create the database can be found [here](https://gitlab.pik-potsdam.de/belmin/livwelldata-paper/-/blob/main/analysis/preprocessings/create_database.Rmd)

The code for the validation can be found [here](https://gitlab.pik-potsdam.de/belmin/livwelldata-paper/-/blob/main/analysis/preprocessings/validation.Rmd)

The reproducible manuscript can be found [here](https://gitlab.pik-potsdam.de/belmin/livwelldata-paper/-/tree/main/analysis/paper/livwell_paper.Rmd)

The companion R package *livwelldata* can be downloaded following the instruction from this git repository: https://gitlab.pik-potsdam.de/belmin/livwelldata 
