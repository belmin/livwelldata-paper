# LivWelldata paper Licenses

The database LivWell may be shared and adapted under the conditions of the [CC-BY 4.0 License](https://creativecommons.org/licenses/by/4.0/). 

The code present in this repository may be shared and adapted under the condition of the [GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/). 

